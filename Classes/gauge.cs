﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

/* --------------------------------------------------------------------------------------
 *  Gauge Class
    balsamino.com

    GNU General Public License
    For the explanation go to the article
    http://www.balsamino.com/component/k2/20-highcharts-databinding-in-c.html"
 * 
 *--------------------------------------------------------------------------------------*/


namespace ChartWidgets
{
    public class Gauge
    {

        public Gauge(String mName)
        {
            Name = mName;
        }

        public enum gaugeValues : int
        {
            // Enum types for the gauge.
            // Here you can define you own values
            
            novalues = 0,
            verypoor = 1,
            poor = 2,
            fair = 3,
            good = 4,
            excellent = 5
        }

        public string Name
        {
            get;
            private set;
        }

        public gaugeValues Value
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Legend
        {
            get;
            set;
        }


        public string ToHtmlString()
        {
            // Here we render the gauge control

            // images taken from http://www.optixl.com/gauge/index.php

            StringBuilder strGauge = new StringBuilder();

            string imgReference = "";
            string suffix = ".png";
            string imgDir = "~/Images/Gauge/";

            imgReference = (HttpContext.Current.Handler as Page).ResolveUrl(imgDir + Value.ToString() + suffix);

            // Container
            strGauge.Append("<div id='Gauge_Container'>\n");
            
            // Title
            strGauge.Append("<div id='Gauge_Title' class='gTitle'>\n");
            strGauge.Append(this.Title);
            strGauge.Append("</div>\n");
            
            // Image
            strGauge.Append("<div id='gauge'>\n");
            strGauge.Append("<img ID='");
            strGauge.Append(this.Name);
            strGauge.Append("' runat='server'");
            strGauge.Append("src='");
            strGauge.Append(imgReference);
            strGauge.Append("' />\n");
            strGauge.Append("</div>\n");

            // Legend
            strGauge.Append("<div id='gLegend' class='gLegend'>\n");
            strGauge.Append(this.Legend);
            strGauge.Append("</div>\n");

            strGauge.Append("</div>\n");

            return strGauge.ToString();
        }


    }

}