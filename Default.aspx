﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="balsamino_com._Default" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">


    <div class="title-area bg-white">
        <h1 class="title-area-title fg-black">balsamino.com</h1>
        <h3 class="title-area-title fg-blue">blog Examples</h3>

    </div>

    <div id="Div3" class="tile-group four" runat="server">
        <div class="tile-group-title fg-darkBlue">
            Examples
        </div>
        
        <a id="A3" class="tile double bg-darkBlue" data-click="transform" runat="server" href="~/examples/hghchrtsSimpleBind.aspx">
            <div id="Div4" class="tile-content icon" runat="server">
                <span class="icon"><i class="icon-stats-3"></i></span>
            </div>
            <div id="Div5" class="tile-status" runat="server">
                <span class="name">HighCharts.net Simple Databind</span>
            </div>
        </a>
        
        <a id="A1" class="tile bg-darkGreen" data-click="transform" runat="server" href="~/examples/sqlEditor.aspx">
            <div id="Div1" class="tile-content icon" runat="server">
                <span class="icon"><i class="icon-database"></i></span>
            </div>
            <div id="Div2" class="tile-status" runat="server">
                <span class="name">C# SQl Editor</span>
            </div>
        </a>
        
        <a id="A4" class="tile double bg-amber" data-click="transform" runat="server" href="~/examples/hghchrtUpdatePanel.aspx">
            <div id="Div9" class="tile-content icon" runat="server">
                <span class="icon"><i class="icon-stats-3"></i></span>
            </div>
            <div id="Div10" class="tile-status" runat="server">
                <span class="name">HighCharts.net UpdatePanel 1</span>
            </div>
        </a>
        
        <a id="A5" class="tile double bg-brown" data-click="transform" runat="server" href="~/examples/hghchrtsUpdpnlAnswer.aspx">
            <div id="Div12" class="tile-content icon" runat="server">
                <span class="icon"><i class="icon-stats-3"></i></span>
            </div>
            <div id="Div13" class="tile-status" runat="server">
                <span class="name">HighCharts.net UpdatePanel 2</span>
            </div>
        </a>

        <a id="A6" class="tile double bg-brown" data-click="transform" runat="server" href="~/examples/hghchrtSpiderWeb.aspx">
            <div id="Div14" class="tile-content icon" runat="server">
                <span class="icon"><i class="icon-stats-3"></i></span>
            </div>
            <div id="Div15" class="tile-status" runat="server">
                <span class="name">HighCharts.net SpiderWeb</span>
            </div>
        </a>

        <a id="A7" class="tile bg-yellow" data-click="transform" runat="server" href="~/examples/gaugeExample.aspx">
            <div id="Div16" class="tile-content icon" runat="server">
                <span class="icon"><i class="icon-stats-3"></i></span>
            </div>
            <div id="Div17" class="tile-status" runat="server">
                <span class="name">Simple Gauge</span>
            </div>
        </a>

    </div>
    <div id="Div8" class="tile-group two" runat="server">
    </div>

    <asp:Timer ID="Timer2" Interval="1000" runat="server">
    </asp:Timer>

    <div id="Div11" class="tile-group two" runat="server">
        <asp:UpdatePanel ID="upd1" runat="server">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer2" />

            </Triggers>
            <ContentTemplate>



                <div class="tile double bg-steel" style="overflow: visible">
                    <div class="tile-content" style="overflow: visible">
                        <div class="text-right padding10 ntp">
                            <span class="fg-white no-margin"><%= System.DateTime.Now.DayOfWeek.ToString() %></span><br />
                            <span class="fg-white no-margin"><%= System.DateTime.Now.ToString("dd/MM/yyyy") %></span><br />
                            <span class="fg-white no-margin"><%= System.DateTime.Now.ToString("HH:mm:ss") %></span>
                        </div>
                    </div>
                    <div class="brand">
                        <div class="label">
                            <h3 class="no-margin fg-white"><span class="icon-calendar"></span></h3>
                        </div>
                    </div>
                </div>

                <!-- end tile -->


            </ContentTemplate>
        </asp:UpdatePanel>

        <a id="A2" class="tile bg-darkGreen" runat="server" data-click="transform" href="About.aspx">
            <div id="Div6" class="tile-content icon" runat="server">
                <span id="Span1" class="icon-help" runat="server"></span>
            </div>
            <div id="Div7" class="tile-status" runat="server">
                <span class="name">About</span>
            </div>
        </a>
    </div>

</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeadContent">


</asp:Content>

