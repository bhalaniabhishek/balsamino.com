﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="gaugeExample.aspx.cs" Inherits="balsamino_com.examples.gaugeExample" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

    <!-- Define a style for the gauges -->
    <style type="text/css">
        .box {
            background-color: white;
            -moz-background-clip: padding; /* Firefox 3.6 */
            -webkit-background-clip: padding; /* Safari 4? Chrome 6? */
            background-clip: padding-box; /* Firefox 4, Safari 5, Opera 10, IE 9 */
            border: 1px solid rgba(0,0,0,0.3);
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            padding: 10px;
            text-align: left;
            font-size: x-small;
            float: left;
            width: 200px;
            margin-left: 10px;
            margin-bottom: 10px;
        }

        .gTitle {
            font-weight: bold;
            color: navy;
            font-size: medium;
        }

        .gLegend {
            color: brown;
            font-size: medium;
        }
    </style>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Gauge Class Example</h1>
    <h2>balsamino.com</h2>

    <h3>GNU General Public License</h3>
    <span>For the explanation go to the article: <a href="http://www.balsamino.com/programming/38-simple-gauge-class-in-c.html" target="_blank">Simple Gauge Class in C#</a></span>

    <div class="margin20">

        <asp:Literal ID="gaugeContainer" runat="server"></asp:Literal>

    </div>
</asp:Content>
