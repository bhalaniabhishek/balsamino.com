﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ChartWidgets;

namespace balsamino_com.examples
{
    public partial class gaugeExample : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // caall the render gauge method
            renderGauge();
        }

        protected void renderGauge()
        {
            // instantiate the gauge object
            Gauge mySimpleGauge = new Gauge("example");

            // set the title
            mySimpleGauge.Title = "Gauge Titile";

            // set the legend
            mySimpleGauge.Legend = "Gauge Legend";

            // set the value
            mySimpleGauge.Value = Gauge.gaugeValues.good;

            // render into the literal container
            gaugeContainer.Text = mySimpleGauge.ToHtmlString();
        
            // that's all
        }



    }
}