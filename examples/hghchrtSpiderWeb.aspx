﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="hghchrtSpiderWeb.aspx.cs" Inherits="balsamino_com.examples.hghchrtSpiderWeb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

    <script src="../Scripts/metro-ui/metro.min.js"></script>

    <script src="../Scripts/jquery-2.0.3.min.js" type="text/javascript"></script>

    <!---------------------------------------------------
        NOTE: 
        The javascript to import is the highcharts-all.js 

    ------------------------------------------------------>
    <script src="../Scripts/Highcharts-4.0.1/js/highcharts-all.js" type="text/javascript"></script>
    <script src="../Scripts/js/exporting.src.js" type="text/javascript"></script>
    <script src="../Scripts/js/export-csv.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <h1>SpiderWeb Chart</h1>
    <h2>balsamino.com</h2>

    <h3>GNU General Public License</h3>
    <span>For the explanation go to the article: <a href="http://www.balsamino.com/component/k2/20-highcharts-databinding-in-c.html" target="_blank">dotnet HighCharts SpiderWeb</a></span>

    <h2 class="title-area-title fg-blue">Pizza SpiderWeb Chart</h2>

    <asp:Literal ID="chrtMyChart" runat="server"></asp:Literal>

</asp:Content>
