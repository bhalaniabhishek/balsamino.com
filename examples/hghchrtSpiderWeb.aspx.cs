﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;

using System.Web.Configuration;

namespace balsamino_com.examples
{
    public partial class hghchrtSpiderWeb : System.Web.UI.Page
    {
        protected string constring = WebConfigurationManager.ConnectionStrings["exportDB"].ToString();
        

        protected void Page_Load(object sender, EventArgs e)
        {

            // Let's define a query string
            string myQuery = "select pizza, count(pizza) from pizzadb group by pizza";

            //calling the renderChart method
            renderChart(fetchData(myQuery), "Pizza in the world", "Spiderweb Comparison", "chart1", chrtMyChart);

        }


        protected List<List<Object>> fetchData(string myQuery)
        {
            // fetchData get the data from the database
            // and put it into a list of list object

            SqlConnection myConn = new SqlConnection(constring);
            SqlCommand cmd = new SqlCommand(myQuery, myConn);
            cmd.CommandType = CommandType.Text;

            myConn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            List<List<Object>> dati = new List<List<object>>();

            List<Object> serie1 = new List<Object>();
            List<Object> yAxis = new List<Object>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    yAxis.Add(reader.GetValue(0));
                    serie1.Add((reader.GetValue(1) == null ? 0 : reader.GetValue(1)));
                }
            }

            else
            {
                Console.WriteLine("No rows found.");
            }
            reader.Close();

            dati.Add(yAxis);
            dati.Add(serie1);

            return dati;
        }



        protected void renderChart(List<List<Object>> values, string title, string subtitle, string chartId, Literal ltrRendering)
        {

            List<Object> serie1 = new List<Object>();
            List<string> yAxis = new List<string>();

            serie1.AddRange(values[1].ToList());

            foreach (object s in values[0].ToList())
                yAxis.Add(s.ToString());

            DotNet.Highcharts.Highcharts chart = new DotNet.Highcharts.Highcharts(chartId)

                // Note the Polar flag
                .InitChart(new Chart 
                { 
                    Polar = true, 
                    Type = ChartTypes.Line 
                })
                
                .SetTitle(new Title { Text = title })
                
                .SetSubtitle(new Subtitle { Text = subtitle })
                
                .SetPane(new Pane { Size = new PercentageOrPixel(80, true) })

                .SetXAxis(new XAxis
                {
                    Categories = yAxis.ToArray(),
                    TickmarkPlacement = Placement.On,
                    LineWidth = 0,
                    Min = 0,
                })
                
                .SetYAxis(new YAxis
                {
                    // Note the "poligon" interpolation for the grid
                    GridLineInterpolation = "polygon",
                    LineWidth = 0,
                    Min = 0,
                })

                .SetTooltip(new Tooltip
                {
                    Shared = true,
                })
                
                .SetLegend(new Legend
                {
                    Align = HorizontalAligns.Right,
                    VerticalAlign = VerticalAligns.Top,
                    Y = 100,
                    Layout = Layouts.Vertical
                })
                
                .SetSeries(new[]
                {
                    new Series
                    {
                        Name = title,
                        Data = new Data(serie1.ToArray()),
                        PlotOptionsLine = new PlotOptionsLine
                        { 
                            PointPlacement = new PointPlacement(Placement.On)
                        
                        }
                    }
                })
                
                .SetCredits(new Credits
                {
                    Enabled = false
                });

            chart.SetLabels(new Labels { });
            chart.SetLegend(new Legend { Enabled = false });
            
            ltrRendering.Text = chart.ToHtmlString();
        }


    }
}