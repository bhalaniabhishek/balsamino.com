﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="hghchrtUpdatePanel.aspx.cs" Inherits="balsamino_com.examples.hghchrtUpdatePanel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <h2>dotnet.HighCharts inside UpdatePanel</h2>
    <h3>balsamino.com</h3>

    <h3>GNU General Public License</h3>
    <span>For the explanation go to the article: <a href="http://www.balsamino.com/component/k2/35-dotnet-highcharts-inside-updatepanel.html" target="_blank">dotnet.HighCharts inside UpdatePanel</a></span>

    <div class="margin20">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
        <script type="text/javascript" src="http://code.highcharts.com/modules/exporting.js"></script>

        <asp:UpdatePanel ID="updPanel" runat="server">
            <ContentTemplate>
                <asp:DropDownList ID="cmbPizza" DataSourceID="sqlPizza" DataTextField="pizza" DataValueField="pizza" OnDataBound="cmbPizza_DataBound" runat="server"></asp:DropDownList>
                <asp:Button ID="btnRefresh" Text="Refresh" OnClick="btnRefresh_Click" runat="server" />

                <div>
                    <asp:Literal ID="chrtMyChart" runat="server"></asp:Literal>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <asp:SqlDataSource ID="sqlPizza" runat="server"
        ConnectionString="<%$ ConnectionStrings:exportDB %>"
        SelectCommand="SELECT distinct pizza from pizzaDB order by pizza"></asp:SqlDataSource>



</asp:Content>
