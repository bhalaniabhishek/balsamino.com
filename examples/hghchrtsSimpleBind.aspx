﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="hghchrtsSimpleBind.aspx.cs" Inherits="balsamino_com.examples.hghchrtsSimpleBind" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <h1>SQL Editor</h1>
    <h2>balsamino.com</h2>

    <h3>GNU General Public License</h3>
    <span>For the explanation go to the article: <a href="http://www.balsamino.com/component/k2/20-highcharts-databinding-in-c.html" target="_blank">C# Sql Editor</a></span>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="http://code.highcharts.com/modules/exporting.js"></script>


    <div>
        <asp:Literal ID="chrtMyChart" runat="server"></asp:Literal>
    </div>

</asp:Content>
