﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

// Include the HighCharts controls
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace balsamino_com.examples
{
    public partial class hghchrtsSimpleBind : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Render_Chart();
        }


        protected void Render_Chart()
        {

            // define the connection string
            string constring = WebConfigurationManager.ConnectionStrings["exportDB"].ToString();
    
            // Declare the SQL connection
      
            SqlConnection myConn = new SqlConnection(constring);
      
            // and add a query string for retrieving the data.
      
            string commandText = "select period, count(pizza) from pizzaDB group by period order by period";
      
            SqlCommand myComm = new SqlCommand(commandText, myConn);
 
            // Open the connection
            myConn.Open();
      
            // and execute the query
            SqlDataReader reader = myComm.ExecuteReader();
 
            Object[] chartValues = new Object[12]; // declare an object for the chart rendering  
            if (reader.HasRows)
            {
                    while (reader.Read())
                    {
                        // GetValue() returns the data row from the query
                        // So:
                        //       GetValue(0) will contain the month number [<em>month(eaten_Pizza) as Mese</em>]
                        //       GetValue(1) will contain the number of eaten pizzas [<em>count(eaten_Pizza)</em>]
       
                        chartValues[(Int32)reader.GetValue(0)-1] = reader.GetValue(1);
                        // minus 1 because the array starts from 0, whenever the months start from 1
                    }
      
            }
            else
            {
                    Console.WriteLine("No rows found.");
            }
      
            reader.Close(); // close the reader
 
            // Declare the HighCharts object    
            DotNet.Highcharts.Highcharts chart = new DotNet.Highcharts.Highcharts("chart")
                .InitChart(new Chart 
                {
                    DefaultSeriesType = ChartTypes.Column 
                })
                .SetTitle(new Title
                {
                    Text = "Monthly Number of Pizza",
                    X = -20
                })
                .SetSubtitle(new Subtitle
                {
                    Text = "Source: Pizza Connect db",
                    X = -20
                })
                .SetXAxis(new XAxis
                {
                    Categories = new[] {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" }
                })
                .SetSeries(new[]
                        {
                        new Series
                        {
                            Name = "# Pizza",
                            Data = new Data(chartValues)   // Here we put the dbase data into the chart                   
                        },
                    });
             
      
            chrtMyChart.Text = chart.ToHtmlString(); // Let's visualize the chart into the webform.
 
        }
        }


}

