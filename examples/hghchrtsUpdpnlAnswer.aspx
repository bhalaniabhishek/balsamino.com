﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="hghchrtsUpdpnlAnswer.aspx.cs" Inherits="balsamino_com.examples.hghchrtsUpdpnlAnswer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="http://code.highcharts.com/modules/exporting.js"></script>

    <h2>Update an dotnet HighCharts Chart every N seconds</h2>
    <h3>balsamino.com</h3>

    <h3>GNU General Public License</h3>
    <span>For the explanation go to the article: <a href="http://www.balsamino.com/component/k2/36-update-a-dotnet-highcharts-chart-every-n-seconds.html" target="_blank">dotnet.HighCharts inside UpdatePanel</a></span>

    <div class="margin20">
        <asp:Timer ID="Timer1" runat="server" Interval="5000" OnTick="Timer1_Tick"></asp:Timer>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <asp:Literal ID="chrt" runat="server"></asp:Literal>
                </div>
            </ContentTemplate>

            <%-- Register the trigger as PostBack and not as AsyncPostBack--%>
        </asp:UpdatePanel>
    </div>

</asp:Content>
