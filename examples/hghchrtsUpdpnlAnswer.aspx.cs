﻿using System;
using System.Web.UI;

// Include the HighCharts controls
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;

namespace balsamino_com.examples
{
    public partial class hghchrtsUpdpnlAnswer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Register the trigger as PostBack and not as AsyncPostBack
            ScriptManager.GetCurrent(this).RegisterPostBackControl(Timer1);
            chart1();
        }

        private void chart1()
        {
            Random rand = new Random((int)DateTime.Now.Ticks);
            Object[] val = new Object[12];

            for (int i = 0; i < 12; i++)
            {
                val[i] = rand.Next(1, 100);
            }

            DotNet.Highcharts.Highcharts chart = new DotNet.Highcharts.Highcharts("chart")
                .InitChart(new Chart
                {
                    DefaultSeriesType = ChartTypes.Column
                })
                .SetTitle(new Title
                {
                    Text = "Random Generated numbers",
                    X = -20
                })
                .SetSubtitle(new Subtitle
                {
                    Text = "balsamino.com",
                    X = -20
                })
                .SetXAxis(new XAxis
                {
                    Categories = new[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" }
                })
                .SetSeries(new Series
                {
                    Data = new Data(val)
                });
            
            chrt.Text = chart.ToHtmlString();
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            chart1();
        }


    }
}