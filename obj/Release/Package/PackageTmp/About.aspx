﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="balsamino_com.About" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<h2>Welcome to my blog <a href="http://www.balsamino.com" target="_blank">balsamino.com</a></h2>

<p>My name is Francesco and I live in a fantastic country called Italy.</p>
<p>
My story starts in the 80th when it begins my passion with the electronics and after with the computer programming with a Z80 computer built in kit.

In these years a new revolutionary computer inspired me and a lot of us: I'm speaking about the Commodore 64.</p>
<p>
I'm and electronic engineer specialized in Telco, but even my job is almost managerial, in my free time I like to play with my toys: mcus, electronic, web, programming and... cutting my green field.
</p>
<p>

The aim of this blog is to share with you ideas, projects, resources about electronic and IT in a collaborative way.</p>
<br />
<h3 class="fg-active-blue">I'm strenghtly convinced that the knlowledge is a common value.</h3>
<p>
So enjoy free to fork, branch and modify the code.
</p>
<p>
I only ask you to share the source (my name in this case) if you modify or use those pieces of code.
</p>
<br />

<p>All the code here is provided under <a href="http://en.wikipedia.org/wiki/GNU_General_Public_License#Version_3" target="_blank">GNU General Public License</a></p>

<img src="Images/GPLv3_Logo.svg.png" />
</asp:Content>
