protected void Page_Load(object sender, EventArgs e)
{
    // Se l'utente non � loggato faccio il redirect alla pagina di login
    // poi torno qui
    adminPanel.Visible = false;
    if (Session["ruolo"] != null) // l'utente non � un amministratore (ruolo = 1)
    {

        if (int.Parse(Session["Ruolo"].ToString()) <= 2)
            adminPanel.Visible = true;
    }

    //if (int.Parse(Session["ruolo"].ToString()) != 1)
    //{
    //    Response.Redirect("Default.aspx");
    //}

    bool utente_loggato = false;
    if (Session["user_in"] != null)
    {
        utente_loggato = bool.Parse(Session["user_in"].ToString());
    }
    Session["last_url"] = "~" + Request.Url.AbsolutePath;

    if (!utente_loggato)
    {
        Response.Redirect("~/Account/Authentication.aspx");
    }
    //    Annulla.Text = "Cancel";    
}