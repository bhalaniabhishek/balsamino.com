﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="sqlEditor.aspx.cs" Inherits="balsamino_com.sqlEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <h1>SQL Editor</h1>
    <h2>balsamino.com</h2>

    <h3>GNU General Public License</h3>
<span>For the explain go tho the articel: <a href="http://www.balsamino.com/component/k2/33-c-dotnet-backend-sql-editor.html" target="_blank">C# Sql Editor</a></span>
    <asp:UpdatePanel ID="updPanel" runat="server">
        <ContentTemplate>

            <asp:HiddenField ID="hdnConstring" runat="server" />


            <div class="grid">

                <div class="row">
                    <div class="span2">
                        <small>dB selection</small>
                        <div>
                            <asp:ListBox ID="lstConnections" runat="server" OnSelectedIndexChanged="lstDBConnections" AutoPostBack="true"></asp:ListBox>
                        </div>
                    </div>

                    <div class="span2">
                        <small>Tables</small>
                        <div>
                            <asp:ListBox ID="lstTables" runat="server" Height="200px" CssClass="small" AutoPostBack="true" OnSelectedIndexChanged="lstTableSelect"></asp:ListBox>
                        </div>
                    </div>

                    <div class="span2">
                        <small>Fields</small>
                        <div>
                            <asp:ListBox ID="lstFields" runat="server" Height="200px" CssClass="small-size" AutoPostBack="true"></asp:ListBox>
                        </div>
                    </div>

                    <div class="span2">
                        <small>StoredProcedures</small>
                        <div>
                            <asp:ListBox ID="lstSP" runat="server" Height="200px" CssClass="small-size" AutoPostBack="true" OnSelectedIndexChanged="lstSPSelect" ></asp:ListBox>
                        </div>
                    </div>

                    <div class="span2">
                        <small>Views</small>
                        <div>
                            <asp:ListBox ID="lstViews" runat="server" Height="200px" CssClass="small-size" AutoPostBack="true" OnSelectedIndexChanged="lstViewsSelect"></asp:ListBox>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div>
                    <small>Query</small>
                </div>
                <div>
                    <asp:TextBox ID="txtQuery" TextMode="MultiLine" Width="100%" Height="200px" runat="server"></asp:TextBox>
                </div>

            </div>
            <div class="row">
                <div>
                    <asp:Button ID="btnCommit" runat="server" OnClick="btnCommitClick" Text="Query" />
                    <asp:Button ID="btnClear" runat="server" OnClick="btnClearClick" Text="Clear" />
                    <asp:Button ID="btnExport" runat="server" OnClick="btnExportClick" Text="Export" />
                </div>
            </div>

            <asp:Label ID="lblInfo" runat="server" Text=""></asp:Label>

            <div class="row">
                <div>
                    <asp:GridView ID="grdResult" runat="server" AutoGenerateColumns="true" AllowSorting="true" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
                        RowStyle-BackColor="#A1DCF2" AlternatingRowStyle-BackColor="White" AlternatingRowStyle-ForeColor="#000"
                        AllowCustomPaging="false" EnableSortingAndPagingCallbacks="true" AllowPaging="false">
                    </asp:GridView>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
